from .models import login
from django import forms

class loginform(forms.ModelForm):
    class meta:
        model=login
        fields = '__all__'
