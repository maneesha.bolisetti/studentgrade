# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db.models.deletion import CASCADE
from django.db import models

# Create your models here.
from django.db import models

# Create your models here.
class Grade(models.Model):
    name = models.CharField(max_length=10, unique=True)
    
    def __str__(self):
        return self.name

class section(models.Model):
    Grade=models.ForeignKey(Grade,on_delete=CASCADE,related_name="Grade")    
    secname=models.CharField(max_length=20)
    
    def __str__(self):
        return self.secname
    
class subject(models.Model):
    section=models.ForeignKey(section,on_delete=CASCADE,related_name="section")
    subname=models.CharField(max_length=20)

    def __str__(self):
        return self.subname
    
class login(models.Model):
    name=models.CharField(max_length=20)
    password=models.SlugField()
    
    def __str__(self):
        return self.name
    
    