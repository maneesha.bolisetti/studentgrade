# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.

from grade.models import Grade,section,subject

# Register your models here.
class GradeAdmin(admin.ModelAdmin):
    list_display=['name']
admin.site.register(Grade,GradeAdmin)

class sectionAdmin(admin.ModelAdmin):
    list_display=['secname','Grade']
admin.site.register(section,sectionAdmin)

class subjectAdmin(admin.ModelAdmin):
    list_display=['subname','section']
admin.site.register(subject,subjectAdmin)