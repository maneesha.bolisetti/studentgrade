# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from grade.models import login
from grade.forms import loginform
from django.shortcuts import render,redirect

def insert_view(request):
   
    if request.method == 'POST':
        form = loginform(request.POST)
        if form.is_valid():
            form.save()
    return render(request,'star/insert.html',{'form':form})
